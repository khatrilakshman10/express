import { Router } from "express"


export let bikeRouter = Router()
bikeRouter
  .route("/")
  .post((req,res,next)=>{
    res.json("bike post")
  })
  .get((req,res,next)=>{
    console.log(req.body)
    res.json("bike get")
  }) 
  .patch((req,res,next)=>{
    res.json("bike update")
  })
  .delete((req,res,next)=>{
    res.json("bike delete")
  })
  